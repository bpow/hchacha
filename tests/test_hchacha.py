import hchacha
from io import StringIO
from textwrap import dedent
from pathlib import Path

DATADIR = Path(__file__).parent.resolve() / "data"


def test_version():
    print(__file__)
    print(Path(__file__).parent)
    assert hchacha.__version__ == '0.1.0'

def test_fasta(capsys):
    fastas = {}
    for f in DATADIR.glob('*.fasta'):
        with open(f) as inf:
            fastas[f.stem] = inf.read()

    for from_scheme, data in fastas.items():
        for target_scheme in fastas:
            hchacha.fasta(inf=StringIO(data), target=target_scheme)
            out, err = capsys.readouterr()
            assert out == fastas[target_scheme]