#!/bin/bash

base=https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/405

for assembly in $(cat <<EOASSEMBLIES
GCF_000001405.13_GRCh37
GCF_000001405.14_GRCh37.p2
GCF_000001405.17_GRCh37.p5
GCF_000001405.21_GRCh37.p9
GCF_000001405.22_GRCh37.p10
GCF_000001405.23_GRCh37.p11
GCF_000001405.24_GRCh37.p12
GCF_000001405.25_GRCh37.p13
EOASSEMBLIES
); do
	curl ${base}/${assembly}/${assembly}_assembly_report.txt
done | grep -v '^#' | sed -e 's/\r//g' | awk '$7 != "NA" && $10 != "na"' | sort | uniq | gzip -9 > src/hchacha/data/GRCh37.tsv.gz


for assembly in $(cat <<EOASSEMBLIES
GCF_000001405.26_GRCh38
GCF_000001405.27_GRCh38.p1
GCF_000001405.28_GRCh38.p2
GCF_000001405.29_GRCh38.p3
GCF_000001405.30_GRCh38.p4
GCF_000001405.31_GRCh38.p5
GCF_000001405.32_GRCh38.p6
GCF_000001405.33_GRCh38.p7
GCF_000001405.34_GRCh38.p8
GCF_000001405.35_GRCh38.p9
GCF_000001405.36_GRCh38.p10
GCF_000001405.37_GRCh38.p11
GCF_000001405.38_GRCh38.p12
GCF_000001405.39_GRCh38.p13
GCF_000001405.40_GRCh38.p14
EOASSEMBLIES
); do
	curl ${base}/${assembly}/${assembly}_assembly_report.txt
done | grep -v '^#' | sed -e 's/\r//g' | awk '$7 != "NA" && $10 != "na"' | sort | uniq | gzip -9 > src/hchacha/data/GRCh38.tsv.gz

