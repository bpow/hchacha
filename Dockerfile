ARG BASE_VERSION=3.11-alpine3.18

# just so we can use the same base image multiple times
FROM python:${BASE_VERSION} AS base

### builder for samtools
FROM base AS samtools_builder
ARG SAMTOOLS_VERSION=1.17

# samtools needs to go into a container with same versions of libz, libbz2 and liblzma (which python:alpine should have)
# Note: samtools is Copyright (C) 2008-2023 Genome Research Ltd., available under MIT/Expat license
RUN apk update \
 && apk add --virtual build-dependencies pcre-dev openssl-dev zlib-dev bzip2-dev xz-dev build-base curl \
 && curl -L -o samtools-${SAMTOOLS_VERSION}.tar.bz2 \
    https://github.com/samtools/samtools/releases/download/${SAMTOOLS_VERSION}/samtools-${SAMTOOLS_VERSION}.tar.bz2 \
 && tar xjf samtools-${SAMTOOLS_VERSION}.tar.bz2  \
 && cd samtools-${SAMTOOLS_VERSION}/ \
 && ./configure --without-curses \
 && make \
 && make install \
 && strip /usr/local/bin/samtools

### builder for hchacha
FROM base AS hchacha_builder

WORKDIR /app
COPY . /app
RUN apk add --update poetry
RUN poetry build

### now the actual image
FROM base

LABEL maintainer="Bradford Powell <bpow@unc.edu>"
LABEL description="A simpplistic tool to convert among naming systems for human chromosome sequencees of the same assembly."

COPY --from=samtools_builder /usr/local/bin/samtools /usr/bin/samtools
COPY . /app
COPY --from=hchacha_builder /app/dist/*.whl /tmp
RUN pip install /tmp/*.whl

ENTRYPOINT ["/usr/local/bin/hchacha"]

